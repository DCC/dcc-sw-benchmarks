#!/bin/bash

# Initialize global variables
if [ $# -eq 1 ]; then
    export BENCHMARK_SW=$1
else
    export BENCHMARK_SW=local
fi

# Get folder name
_b_script=$(realpath ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})
_b_dir=$(dirname $_b_script)
_b_home=$_b_dir/../..
pushd $_b_home

d=runs
mkdir -p $d
pushd $d

# Submit all benchmarks for the DCC cluster setup
module purge

# Figure out local computer size
info=$(lscpu -e | tail -n +2 | awk 'BEGIN {node=0 ; socket=0 ; core=0} {
node=($2>node ? $2 : node)
socket=($3>socket ? $3 : socket)
core=($4>core ? $4 : core)
 } END {print node+1,socket+1,core+1}')

nodes=$(echo $info | awk '{print $1}')
sockets=$(echo $info | awk '{print $2}')
cpus=$(echo $info | awk '{print $3}')

export BENCHMARK_SW_PROCS=$((nodes*sockets*cpus))
export BENCHMARK_SW_NODES=$nodes
export BENCHMARK_SW_PPS=$cpus

# Clean environment
module purge
env

dir=local

mkdir -p $dir

unset OMP_NUM_THREADS
export BENCHMARK_SW_MPI_OPTS='--report-bindings'

pushd $dir
bash $_b_home/benchmarks/run.sh procs-$BENCHMARK_SW_PROCS-nodes-$BENCHMARK_SW_NODES

popd

popd
popd
