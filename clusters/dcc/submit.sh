#!/bin/bash

# Script takes 2 arguments
#
# 1. software version
# 2. compiler version
#
# $0 2019-jun intel/19.0.3

if [ $# -ne 2 ]; then
    echo "Could not find software stack version!"
    echo "Could not find software stack compiler version!"
    exit 1
fi

# Initialize global variables
export BENCHMARK_SW=dcc
sw_version=$1
sw_compiler_version=$2
shift 2

# Get folder name
_b_script=$(realpath ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})
_b_dir=$(dirname $_b_script)
_b_home=$_b_dir/../..
pushd $_b_home

d=runs
mkdir -p $d
pushd $d

# Submit all benchmarks for the DCC cluster setup
module purge

# Read in lines in the config file
while IFS= read -r line ;
do
    [ -z "$line" ] && continue
    [ "${line:0:1}" == "#" ] && continue

    # We have a valid line
    echo $line
    declare -A info
    # Fill info with default values
    info[mem]=3500MB
    info[time]=15:00
    info[queue]=hpc
    info[ppn]=8
    info[max_nodes]=2

    # Read in values
    for kv in $line
    do
	k=${kv%=*}
	v=${kv#*=}
	info[$k]=$v
    done
    
    # Now create submit script
    for nodes in $(seq 1 ${info[max_nodes]})
    do
	n=$((${info[ppn]}*nodes))
	sed -e "
s!@arch@!${info[arch]}!g;
s!@mem@!${info[mem]}!g;
s!@ppn@!${info[ppn]}!g;
s!@n@!$n!g;
s!@time@!${info[time]}!g;
s!@queue@!${info[queue]}!g;
s!@sw-version@!$sw_version!g;
s!@sw-compiler-version@!$sw_compiler_version!g;
s!@directory@!$_b_home!g;
" $_b_dir/base.bsub > tmp.bsub
	bsub < tmp.bsub
    done
    
done < $_b_dir/config


popd
popd
