# Different software stacks benchmarks

Each directory tracks a different software stack maintained.

## Controls for the benchmarks

Some benchmarks are not meant for running on multiple nodes/processors.
In that case we have a set of environment variables that benchmarks may
use to control how they are executed, and if they are executed.

The following environment variables should be made available in the
submit script:

- BENCHMARK_SW_PROCS
  total number of processors running in the job
- BENCHMARK_SW_NODES
  number of nodes the benchmark is runned on
- BENCHMARK_SW_PPS
  number of processors on a single socket
- BENCHMARK_SW_TEST
  if a value is 1 is supplied, then nothing will be runned,
  but everything up till the *actual* benchmark will be created
  
	
