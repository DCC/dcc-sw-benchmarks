#
# Declare a function which aids in the creation of
# loops for MPI variants

# Call it like this:
#  declare -A local_hash
#  base=setup_omp_bind local_hash
#  local_hash["${base}n"] # number of different runs
#  local_hash["${base}dir-<$(seq 1 n)>] # directory for the run
#  local_hash["${base}env-<$(seq 1 n)>] # options for MPI

function setup_omp_bind {
    local base="omp-bind-"
    [[ $# -eq 0 ]] && printf '%s' "$base" && return 0
    local symlink=$1
    shift

    eval "$symlink['${base}n']=3"
    eval "$symlink['${base}dir-1']=TRUE"
    eval "$symlink['${base}env-1']='export OMP_PROC_BIND=TRUE'"
    eval "$symlink['${base}dir-2']=SPREAD"
    eval "$symlink['${base}env-2']='export OMP_PROC_BIND=SPREAD'"
    eval "$symlink['${base}dir-3']=FALSE"
    eval "$symlink['${base}env-3']='export OMP_PROC_BIND=FALSE'"
}
