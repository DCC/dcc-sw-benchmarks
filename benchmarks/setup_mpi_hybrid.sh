#
# Declare a function which aids in the creation of
# loops for MPI variants
# This script makes use of the following environment variables
#
# - BENCHMARK_SW_PROCS
# - BENCHMARK_SW_NODES
# - BENCHMARK_SW_PPS

# Call it like this:
#  declare -A local_hash
#  setup_mpi_hybrid local_hash
#  local_hash["mpi-hybrid-n"] # number of different runs
#  local_hash["mpi-hybrid-dir-<$(seq 1 n)>] # directory for the run
#  local_hash["mpi-hybrid-opts-<$(seq 1 n)>] # options for MPI

function setup_mpi_hybrid {
    local base="mpi-hybrid-"
    [[ $# -eq 0 ]] && printf '%s' $base && return 0
    local symlink=$1
    shift

    local -i i
    local div omp mpi

    eval "$symlink['${base}n']=0"

    [ -z "$BENCHMARK_SW_PROCS" ] && return 1
    [ -z "$BENCHMARK_SW_NODES" ] && return 1
    [ -z "$BENCHMARK_SW_PPS" ] && return 1

    i=1
    eval "$symlink['${base}dir-$i']=mpi-$BENCHMARK_SW_PROCS"
    # This is OpenMPI flags (should be changed for MPICH/MVAPICH etc.)
    eval "$symlink['${base}opt-$i']='-np $BENCHMARK_SW_PROCS --map-by core'"
    eval "$symlink['${base}env-$i']='unset OMP_NUM_THREADS'"

    # Create the hash-table
    for div in $(seq 64)
    do
	[[ $div -gt $BENCHMARK_SW_PPS ]] && break
	
	# Calculate settings
	omp=$((BENCHMARK_SW_PPS / div))
	[ $((omp * div)) -ne $BENCHMARK_SW_PPS ] && continue

	mpi=$((BENCHMARK_SW_PROCS / omp))

	let i=i+1
	eval "$symlink['${base}dir-$i']=mpi-$mpi-omp-$omp"
	# This is OpenMPI flags (should be changed for MPICH/MVAPICH etc.)
	eval "$symlink['${base}opt-$i']='--map-by ppr:$div:socket:pe=$omp -x OMP_NUM_THREADS=$omp'"
	eval "$symlink['${base}env-$i']='export OMP_NUM_THREADS=$omp'"

    done
    eval "$symlink['${base}n']=$i"

}
