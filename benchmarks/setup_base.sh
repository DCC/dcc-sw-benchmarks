# Base functions for running benchmarks

# These helper routines are replacement functions which handles options.
# I.e. if a user wishes to do verbose runnings the functions here
# are used for this abstraction

function benchmark_run {
    if [[ 0 -eq 0$BENCHMARK_SW_TEST ]]; then
	# The user is requesting a test
	echo "Will run: $@" >&2
	$@
    else
	# The user is requesting a test
	echo "Would run: $@" >&2
    fi
    return $?
}

