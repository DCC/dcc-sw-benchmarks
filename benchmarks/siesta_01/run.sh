#!/bin/bash

# This script requires some env vars to be defined:
#
#  - BENCHMARK_SW: the software stack
#
# Optional flags used when calling MPI
#
#  - BENCHMARK_SW_MPI_OPTS
#

set -e

# Retrieve filename for current script
# 1. get current script full path
_b_script=$(realpath ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})
# 2. get file directory
BASE_DIR=$(dirname $_b_script)
# 3. Add benchmark functions
source $BASE_DIR/../setup_base.sh

# the basename corresponds to the benchmark name
# benchmark name:
_dir=$(basename $BASE_DIR)


# In case the script is called with additional arguments we will append them
# to the name of the output test.
# This enables fine-tuning of output
while [ $# -gt 0 ]; do
    _dir=$_dir/$1
    shift
done

# Check if the benchmark exists
if [ -e $BASE_DIR/module_$BENCHMARK_SW.sh ]; then
    source $BASE_DIR/module_$BENCHMARK_SW.sh
else
    echo "Could not succesfully run: $BASE_DIR"
    echo "Missing $BASE_DIR/module_$BENCHMARK_SW.sh file"
    exit 1
fi



# Define benchmark
function run {
    module_purge
    module_load


    # Run for all different algorithms
    for method in divide-and-conquer mrrr expert qr elpa-1stage elpa-2stage
    do
    
	mkdir -p run-$method
	pushd run-$method
	ln -s $BASE_DIR/C.psf C.psf
	ln -s $BASE_DIR/STRUCT.fdf STRUCT.fdf
	# Create STRUCT.fdf for the minimal CNT
	{
	    echo "Diag.Algorithm $method"
	    echo "%include $BASE_DIR/Default.fdf"
	} > RUN.fdf
	{
	    benchmark_run mpirun $BENCHMARK_SW_MPI_OPTS $@ $SIESTA RUN.fdf
	} > run.out

	rm -f siesta.*
	
	popd
    done
	
    # Done
    module_purge
}


# Populate functions
source $BASE_DIR/../setup_mpi_hybrid.sh
source $BASE_DIR/../setup_omp_bind.sh

declare -A runs
setup_mpi_hybrid "runs"
mpi_hybrid=$(setup_mpi_hybrid)
setup_omp_bind "runs"
omp_bind=$(setup_omp_bind)


# In case the test-directory already exists we will not start anything.
# So simply quit
if [[ ! -d $_dir ]]; then
    
    # The directory does not exist, create it and go into the directory
    mkdir -p $_dir
    pushd $_dir
    run
    popd

fi

for mpi_hybrid_i in $(seq ${runs[${mpi_hybrid}n]})
do
    # Setup the values
    ${runs[${mpi_hybrid}env-$mpi_hybrid_i]}

    # Source for getting the correct OMP executable
    source $BASE_DIR/module_$BENCHMARK_SW.sh

    if [ -z $OMP_NUM_THREADS ]; then
	_dir_add="${runs[${mpi_hybrid}dir-$mpi_hybrid_i]}"
	if [[ ! -d $_dir-$_dir_add ]]; then
	    # The directory does not exist, create it and go into the directory
	    mkdir -p $_dir-$_dir_add
	    pushd $_dir-$_dir_add
	    run "${runs[${mpi_hybrid}opt-$mpi_hybrid_i]}"
	    popd
	fi

    else
	for omp_bind_i in $(seq ${runs[${omp_bind}n]})
	do
	    ${runs[${omp_bind}env-$omp_bind_i]}
	    
	    _dir_add="${runs[${mpi_hybrid}dir-$mpi_hybrid_i]}-${runs[${omp_bind}dir-$omp_bind_i]}"

	    if [[ ! -d $_dir-$_dir_add ]]; then
		# The directory does not exist, create it and go into the directory
		mkdir -p $_dir-$_dir_add
		pushd $_dir-$_dir_add
		run "${runs[${mpi_hybrid}opt-$mpi_hybrid_i]}" "-x OMP_PROC_BIND"
		popd
	    fi

	    [ $OMP_NUM_THREADS -eq 1 ] && break
	done
    fi
done
