# Siesta CNT benchmark

A benchmark running the a CNT with various diagonalization methods.

This will run the following diagonalizations for direct comparison:

- Divide-and-conquer
- MRRR
- QR
- Expert-driver
- ELPA (1-stage)
- ELPA (2-stage)
