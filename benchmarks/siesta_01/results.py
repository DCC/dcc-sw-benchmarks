import os
import glob

def parse_result(directory):
    """ Returns a dictionary object with sub-dictionaries per method used
    
    Parameters
    ----------
    directory : directory
       the directory containing ``run.out``
    """
    for d in glob.glob(os.path.join(directory, 'run-*')):
        # Retrieve method
        m = {}
        method = os.path.basename(d).split('-', 1)[1]
        m['method'] = method

        # Open TIMES file
        f = open(os.path.join(d, 'TIMES'), 'r')

        for line in f:
            if line.startswith('timer: Total elapsed wall'):
                # Total time
                m['time'] = float(line.split()[-1])
            elif line.startswith('cdiag '):
                # diagonalization time
                vals = line.split()
                m['diagonalization-total'] = float(vals[4])
                m['diagonalization'] = float(vals[4]) / float(vals[1])
        # Calculate complete time
        m['siesta'] = m['time'] - m['diagonalization-total']
        yield m
