# This module requires the following variables

# - BENCHMARK_SW_DCC

if [ -z "$BENCHMARK_SW_DCC" ]; then
    echo "Error in determining SW-version: BENCHMARK_SW_DCC"
    exit 1
fi

function module_purge {
    module purge
    module load $BENCHMARK_SW_DCC-dcc-setup
}

function module_load {
    module load python/3.7.3
    module load numpy
}
