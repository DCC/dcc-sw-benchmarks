from __future__ import print_function, division
import os
import numpy as np
import time as t

for key in os.environ.keys():
    if key.startswith('OMP_'):
        print('{}: {}'.format(key, os.environ[key]))

for N, size in [(20, 300), (10, 1000), (4, 2000), (2, 3000), (1, 5000)]:
    A = np.random.rand(size, size)
    B = np.random.rand(size, size)
    t0 = t.time()
    for _ in range(N):
        C = np.dot(A, B)
    t1 = t.time()
    print('numpy.dot {size}: {t} s'.format(size=size, t=(t1 - t0)/N))
