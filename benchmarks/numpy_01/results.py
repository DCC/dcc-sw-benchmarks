import os

def parse_result(directory):
    """ Returns a dictionary object with OMP_* and timings for each matrix size
    
    Parameters
    ----------
    directory : directory
       the directory containing ``run.out``
    """
    f = open(os.path.join(directory, 'run.out'), 'r')

    result = {}
    for line in f:
        k, v = line.split(':')
        if line.startswith('OMP_'):
            # The top output file contains the OMP_* variables
            result[k] = v.strip()
        else:
            # This is now matrix size and timings
            result[k.split()[1]] = float(v.split()[0])

    yield result
