
function module_purge {
    module purge
}

function module_load {
    module load python/3.7.3
    module load numpy
}
