#!/bin/bash

# This script requires some env vars to be defined:
#
#  - BENCHMARK_SW: the software stack
#

set -e

# Retrieve filename for current script
# 1. get current script full path
_b_script=$(realpath ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})
# 2. get file directory
BASE_DIR=$(dirname $_b_script)
# 3. Add benchmark functions
source $BASE_DIR/../setup_base.sh

# the basename corresponds to the benchmark name
# benchmark name:
_dir=$(basename $BASE_DIR)

# This benchmark will only run on a single node
[ $BENCHMARK_SW_NODES -gt 1 ] && exit 0


# In case the script is called with additional arguments we will append them
# to the name of the output test.
# This enables fine-tuning of output
while [ $# -gt 0 ]; do
    _dir=$_dir/$1
    shift
done

# Check if the benchmark exists
if [ -e $BASE_DIR/module_$BENCHMARK_SW.sh ]; then
    source $BASE_DIR/module_$BENCHMARK_SW.sh
else
    echo "Could not succesfully run: $BASE_DIR"
    echo "Missing $BASE_DIR/module_$BENCHMARK_SW.sh file"
    exit 1
fi


# Define benchmark
function run {
    module_purge
    module_load

    export OMP_NUM_THREADS=$BENCHMARK_SW_PROCS
    {
	benchmark_run python3 $BASE_DIR/run.py
    } > run.out
	
    # Done
    module_purge
}


# Populate functions
source $BASE_DIR/../setup_omp_bind.sh

declare -A runs
setup_omp_bind "runs"
omp_bind=$(setup_omp_bind)

# Default OMP_PLACES to cores, if not set
[ -z ${OMP_PLACES+x} ] && export OMP_PLACES=CORES


# In case the test-directory already exists we will not start anything.
# So simply quit
if [[ ! -d $_dir ]]; then
    
    # The directory does not exist, create it and go into the directory
    mkdir -p $_dir
    pushd $_dir
    run
    popd

fi

for omp_bind_i in $(seq ${runs[${omp_bind}n]})
do
    ${runs[${omp_bind}env-$omp_bind_i]}
    
    dir_add="${runs[${omp_bind}dir-$omp_bind_i]}"
    
    if [[ ! -d $_dir-$_dir_add ]]; then
	# The directory does not exist, create it and go into the directory
	mkdir -p $_dir-$_dir_add
	pushd $_dir-$_dir_add
	run
	popd
    fi
    
done
