#!/bin/bash

# This script requires some env vars to be defined:
#
#  - BENCHMARK_SW: the software stack
#

export SHELL=/bin/bash
set -e

# Print-out the bash version used
echo Bash-version: ${BASH_VERSION}
echo Starting benchmark at $(date)

# Retrieve filename for current script
_b_script=$(realpath ${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]})
BM_DIR=$(dirname $_b_script)

echo Running siesta_01
bash $BM_DIR/siesta_01/run.sh $@
echo Running transiesta_01
bash $BM_DIR/transiesta_01/run.sh $@
echo Running numpy_01
bash $BM_DIR/numpy_01/run.sh $@
echo Running numpy_02
bash $BM_DIR/numpy_02/run.sh $@


echo Ending benchmark at $(date)
