import os
import sys

def _parse_d_values(values):
    """ Simple parser """
    while len(values) > 0:
        v = values.pop(0)
        if v == 'procs':
            yield 'processors', int(values.pop(0))
        elif v == 'nodes':
            yield 'nodes', int(values.pop(0))
        elif v == 'mpi':
            yield 'mpi', int(values.pop(0))
        elif v == 'omp':
            yield 'omp', int(values.pop(0))
        elif v in ['TRUE', 'FALSE', 'SPREAD']:
            yield 'OMP_PROC_BIND', v
    

def parse_result(directory, parser):
    """ Parse results by parsing directories containing environment variables

    Parameters
    ----------
    directory : path
       top-level directory where all directories have names depending on env-vars
    parser : callable
       the parser that returns a dictionary with results
    """

    # Loop directories
    for d in glob.glob(os.path.join(directory, '*')):
        ds = d.split()
        results = {}
        for k, v in _parse_d_values(ds):
            results[k] = v

        # Now parse sub-directory
        for result in parser(d):
            # Create a copy to ensure no left-overs are recursed
            r = dict(results)
            r.update(result)
            yield r
