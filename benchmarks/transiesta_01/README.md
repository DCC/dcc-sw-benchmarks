# Siesta CNT benchmark

A benchmark running the following systems:

1. An electrode calculation (using parallel over k option) of 56 C atoms
2. A device system of elec x 3 with transiesta calculation @ 0 V.
3. A TBtrans calculation for Gamma-point only.
