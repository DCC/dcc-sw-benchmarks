
function module_purge {
    module purge
}

function module_load {
    module load siesta-trunk
}

# Figure out if we are dealing with an openmp run or not!
if [ -z ${OMP_NUM_THREADS+x} ]; then
    SIESTA=siesta
    TBTRANS=tbtrans
else
    SIESTA=siesta_omp
    TBTRANS=tbtrans_omp
fi
