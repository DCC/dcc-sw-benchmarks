import numpy as np
import sisl as si

nt = si.geom.nanotube

C = si.Atom('C')
n = 8
CNTs = [nt(1.45, C, chirality=(n, 1))]
CNT = CNTs[0]
for i in range(2, n+1):
    CNTs.append(nt(1.45, C, chirality=(n, i)))
    if len(CNTs[-1]) < len(CNT) and CNTs[-1].cell[2, 2] > 3:
        CNT = CNTs[-1]
#CNT.write('CNT.xyz')

# Now create an electrode
CNT.write('ELEC.fdf')
CNT.tile(4, 2).write('DEVICE.fdf')


